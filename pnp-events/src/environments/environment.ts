// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyAcR-xHfVHjJbU7-FspIcWz2vchD-6E53M",
    authDomain: "pnp-events-2612d.firebaseapp.com",
    databaseURL: "https://pnp-events-2612d.firebaseio.com",
    projectId: "pnp-events-2612d",
    storageBucket: "pnp-events-2612d.appspot.com",
    messagingSenderId: "314499252983",
    appId: "1:314499252983:web:01e6d9d5b86cad12ba09f2",
    measurementId: "G-929KBPPPE7"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
