import { Component, OnInit } from '@angular/core';
import { Event, Participant } from 'src/app/data-models';
import { ActivatedRoute, Router } from '@angular/router';
import { EventService } from '../../event.service';

@Component({
  selector: 'app-event-details',
  templateUrl: './event-details.component.html',
  styleUrls: ['./event-details.component.css']
})
export class EventDetailsComponent implements OnInit {

  event: Event;
  constructor(
    private route: ActivatedRoute,
    private eventService: EventService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.eventService.getEvent(this.route.snapshot.params.id)
    .then(event => this.event = event as Event);
  }

  viewList() {
    this.router.navigate(['./events']);
  }

  addParticipant(participant: Participant) {
    // console.log(participant);
    this.event.participants.push(participant);
    this.eventService.updateEvent(this.event);
  }
}
