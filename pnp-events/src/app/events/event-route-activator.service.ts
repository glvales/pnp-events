import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { EventService } from '../event.service';

@Injectable({
    providedIn: 'root'
})
export class EventsRouteActivator implements CanActivate {
    constructor(
        private router: Router,
        private eventService: EventService
    ) {}

    canActivate() { //this function will return boolean value, if returned true it will can active the event route
        let isLoggedin: boolean;
        if (this.eventService.userAccount.value.userName === '' && this.eventService.userAccount.value.password === '') {
            isLoggedin = false;
            this.router.navigate(['/login']);
        } else {
            isLoggedin = true;
        }
        return isLoggedin;
    }
}