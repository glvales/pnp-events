import { Component, OnInit } from '@angular/core';
import { UserAccount } from '../data-models';
import { Router} from '@angular/router';
import { EventService } from '../event.service'

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  userName;
  password;

  currentUser: UserAccount = {
    userName: 'junrey',
    password: 'password'
  }

  constructor(
    private router: Router,
    private eventService: EventService
  ) { }

  ngOnInit(): void {
    // this.eventService.currectUserAccount.subscribe(currentUser => {
    //   console.log(currentUser);
    // })
  }

  login(userAccount:UserAccount) {
    if (userAccount.userName === this.currentUser.userName && userAccount.password === this.currentUser.password){
      this.eventService.updateCurrentUser(userAccount);
      console.log('allowed');
      this.router.navigate(['/events']);
      // console.log(userAccount);
    } else {
      // console.log(userAccount);
      console.log('not allowed');
    }
  }
}
